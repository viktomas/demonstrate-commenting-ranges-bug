import * as vscode from 'vscode';

const createCommentingRangeProvider = (): vscode.CommentingRangeProvider => ({
  provideCommentingRanges: (document) => [
    new vscode.Range(
      new vscode.Position(0, 0),
      new vscode.Position(document.lineCount - 1, 0)
    ),
  ],
});

export async function activate(context: vscode.ExtensionContext) {
  const disposable = vscode.commands.registerCommand(
    'commenting-range-bug-demonstration.helloWorld',
    async () => {
      const commentController = vscode.comments.createCommentController(
        'id',
        'label'
      );
      // interruption here causes the bug
      await new Promise((res) => setTimeout(res, 100));
      commentController.commentingRangeProvider =
        createCommentingRangeProvider();
    }
  );

  context.subscriptions.push(disposable);
}
